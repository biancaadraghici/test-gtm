$(document).ready(function () {

    /* -- load function and hover/default button -- */
    var boxList = $("#boxList .box");
    var boxesNr = boxList.length;
    var boxesToShow =  boxList.slice(4,boxesNr).length;
    var loadBtn = $(".load-more");
    var actionsBtn = $("#actions a");
    var hovers = [$(".main .box:nth-child(1) a span"),$("#load"),$(".box:nth-child(1) .btn button")];
    actionsClicked = true;
    loadClicked = true;
    document.getElementById("load").innerHTML += " (+" + boxesToShow + ")";
    boxList.slice(4,boxesNr).hide();
    function btnChange(){
        actionsBtn.css("background-color","#212121").html("Default");
    }
    function btnDefault(){
        actionsBtn.css("background-color", "#616161").html("Hovers");
    }
    function loadElements(){
        boxList.slice(4,boxesNr).show();
        loadBtn.hide();
        btnChange();
    }
    function hideElements(){
        boxList.slice(4,boxesNr).slideUp(400,"linear");
        loadBtn.show();
        btnDefault();
    } 
    function hoverElements(){
        btnChange();
        hovers[0].css("background-color","#DAECFE");
        hovers[1].css({
            "background-color": "#429BF4",
            "color": "#FFFFFF"
        });
        hovers[2].css("background-color", "#3082D4");
    }
    function toDefault(){
        btnDefault();
        hovers[0].css("background-color","#F3F3F3");
        hovers[1].css({
        "background-color": "#FFFFFF",
        "color": "#429BF4"
        });
        hovers[2].css("background-color", "#429BF4");
    }
    loadBtn.on('click', function(e){
        e.preventDefault();
        if(loadClicked)
        {
            loadElements();
            if($(window).width()<999)
            {
                $("#actions").show();
            }
            loadClicked=false;
            actionsClicked=false;
        }else{
            hideElements();
            loadClicked=true;
            actionsClicked=true;
        }
    });
    actionsBtn.on('click',function(e){
        e.preventDefault();
        if(actionsClicked){
            hoverElements();
            actionsClicked=false;
        }else{
            toDefault();
            hideElements();
            actionsClicked=true;
            loadClicked=true;
            if($(window).width()<999)
            {
                $("#actions").hide();
            }
        }
    });

    /* -- popup and copy to clipboard functions -- */

    var popUpBtn = $(".popup");
    var popup = $("#popupBox");
    var success = $("#success");
    popup.hide();
    success.hide();

    function popUp(){
        popup.show();
        hovers[0].css("background-color","#DAECFE");
    }
    function hidePopUp(){
        popup.hide();
        hovers[0].css("background-color","#F3F3F3");
    }
    function copyText(txt){
        var x = document.createElement("textarea");
        document.body.appendChild(x);
        x.value = txt;
        x.select();
        document.execCommand("copy");
        document.body.removeChild(x);
    }
    if($(window).width()>=1000){
        popUpBtn.on('click',function(e){
            e.preventDefault();
            actionsBtn.hide();
            var copyClicked = true;
            popUp();
            var copyBtn = $("svg g.copy");
            copyBtn.css("cursor","pointer");
            var closeBtn = $("svg path.g");
            closeBtn.css("cursor","pointer");
            closeBtn.on('click', function(){
                hidePopUp();
                success.hide();
                copyClicked = true;
                actionsBtn.show();
            });
            copyBtn.on('click',function(){
                
                if(copyClicked)
                {
                    copyText('BONUS77777');
                    success.fadeIn();
                    copyClicked=false;
                }else{
                    success.fadeOut();
                    copyClicked=true;
                }
            });
            
        });
    }else if($(window).width()<999){
        var up = true;
        popUpBtn.on('click',function(e){
            e.preventDefault();
            var copyClicked = true;
            if(up == true){
                popUp();
                var copyBtn = $("svg g.copy");
                copyBtn.css("cursor","pointer");
                copyBtn.on('click',function(){
                    if(copyClicked)
                    {
                        copyText('BONUS77777');
                        success.fadeIn();
                        copyClicked=false;
                    }else{
                        success.fadeOut();
                        copyClicked=true;
                    }
                });
                up = false;
            }else{
                hidePopUp();
                success.hide();
                copyClicked = true;
                up=true;
            }
                
        });
    }

    $(".content.col2 div:nth-child(1)").on('click', function(e){
        e.preventDefault();
    });



    /* -- highlight function -- */
   $("body").click(function(){
       console.log(1);


   });
    


});






    
